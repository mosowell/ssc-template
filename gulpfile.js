let gulp = require('gulp');
let	browserSync = require('browser-Sync').create();
let	scss = require('gulp-sass');
let	sourcemaps = require('gulp-sourcemaps');
let pathToApp = './app/';
let pathToAssets = pathToApp + 'assets/';

gulp.task("server" ,function() {
	browserSync.init(
		{
			proxy: "intervolga.local",
		}
	);

	gulp.watch(pathToApp + 'views/*.php').on('change', browserSync.reload);
	gulp.watch(pathToAssets + 'css/main.css').on('change', browserSync.reload);
	gulp.watch(pathToAssets + 'scss/**/*.scss').on('change', gulp.series('scss'));
});

gulp.task('scss' ,function() {
	gulp.src(pathToAssets +'scss/main.scss')
		.pipe(sourcemaps.init())
		.pipe(scss())
		.pipe(sourcemaps.write())
		.pipe(gulp.dest(pathToAssets +'css'));
});

gulp.task('default', gulp.series('server'));